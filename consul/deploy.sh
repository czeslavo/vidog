#!/bin/sh

rm -rf consul-helm
git clone https://github.com/hashicorp/consul-helm.git && \
cd consul-helm && \
git checkout v0.8.1 && \
helm install --name consul-vidog-prod ./ -f ../values.yml

sleep 5

# README: https://www.consul.io/docs/platform/k8s/dns.html#coredns-configuration
# TODO: describe for kube-dns also

cluster_ip=$(kubectl get svc consul-consul-dns -o jsonpath='{.spec.clusterIP}')
echo "Please set .spec.clusterIp of coredns ConfigMap in kube-system namespace\n"

echo "You need to append consul entry in Corefile configuration as follows:\n"
echo "consul-{namespace} {
      errors
      cache 30
      proxy . ${cluster_ip}
    }"

echo "Command: kubectl edit cm coredns -n kube-system"

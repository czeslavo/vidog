from collections import namedtuple

Environment = namedtuple('Environment', [
    'VIDOG_NAMESPACE',
    'VIDOG_CONSUL_ENABLED',
    'VIDOG_CONSUL_DOMAIN',
    'VIDOG_ANALYSER_IMAGE',
])

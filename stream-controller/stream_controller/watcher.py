from abc import ABCMeta, abstractmethod
from kubernetes import client, watch
from stream_controller.logger import log


class WatchInvalid(Exception):
    pass


class Watcher(metaclass=ABCMeta):
    def __init__(self, api_client, domain, version, namespace, resource):
        self._watcher = watch.Watch()
        self._api_client = api_client or client.ApiClient()
        self._api_func = None
        self._domain = domain
        self._version = version
        self._namespace = namespace
        self._resource = resource

    def watch(self):
        while True:
            try:
                self._watch()
            except WatchInvalid:
                log.debug('Watch invalidated, restarting watch')
                continue

    def _watch(self):
        for event in self._watcher.stream(self._api_func,
                                          self._domain,
                                          self._version,
                                          self._namespace,
                                          self._resource):
            log.debug(event)
            self._dispatch_event(event)

    def _dispatch_event(self, event):
        obj = event['object']
        operation = event['type']

        dispatcher = {
            'ADDED': self.handle_added,
            'DELETED': self.handle_deleted,
            'MODIFIED': self.handle_modified,
            'ERROR': self.handle_error
        }

        def noop(o): return None
        dispatcher.get(operation, noop)(obj)

    @abstractmethod
    def handle_added(self, obj):
        pass

    @abstractmethod
    def handle_modified(self, obj):
        pass

    @abstractmethod
    def handle_deleted(self, obj):
        pass

    def handle_error(self, obj):
        if obj['code'] == 410:
            # It's a common pattern, more on this: https://kubernetes.io/docs/reference/using-api/api-concepts/
            raise WatchInvalid()

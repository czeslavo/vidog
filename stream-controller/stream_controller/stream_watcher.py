from kubernetes import client
from kubernetes.client.rest import ApiException
import yaml
import re

from stream_controller.watcher import Watcher
from stream_controller.logger import log
from stream_controller.stream_components import ComponentDefinitionBuilder, build_component_name


class StreamWatcher(Watcher):
    '''Watches 'streams' CRD and reacts to their changes.'''

    def __init__(self, env, api_client=None):
        super().__init__(api_client=api_client,
                         domain='vidog.io',
                         version='v1',
                         namespace=env.VIDOG_NAMESPACE,
                         resource='streams')
        self._api_func = client.CustomObjectsApi(
            api_client=self._api_client).list_namespaced_custom_object

        self._env = env
        self._core_api = client.CoreV1Api(api_client=self._api_client)

    def handle_added(self, stream):
        '''Checks if defined analysers are created, if not creates them.'''

        stream_meta = stream['metadata']

        if 'analysers' in stream['spec']:
            analysers = stream['spec']['analysers']
            for analyser in analysers:
                self._create_analyser_if_needed(stream_meta, analyser)

        if 'observers' in stream['spec']:
            observers = stream['spec']['observers']
            for observer in observers:
                self._create_observer_if_needed(stream, observer)

    def handle_modified(self, obj):
        pass

    def handle_deleted(self, obj):
        pass

    def _create_analyser_if_needed(self, stream_meta, analyser):
        stream_name = stream_meta['name']
        component_name = f'analyser-{analyser}'
        if not self._component_exists(component_name=component_name, stream_name=stream_name):
            log.info(f"{analyser} will be created for {stream_name}")
            self._create_analyser(stream_meta, analyser)
        else:
            log.debug(f"{analyser} already exists for {stream_name}")

    def _component_exists(self, stream_name, component_name):
        try:
            self._core_api.read_namespaced_pod(
                name=build_component_name(component_name=component_name, stream_name=stream_name), namespace=self._namespace)
            return True
        except ApiException:
            return False

    def _create_analyser(self, stream_meta, analyser):
        pod = ComponentDefinitionBuilder(stream_meta=stream_meta,
                                         component_name=f'analyser-{analyser}',
                                         env=self._env).build()
        self._core_api.create_namespaced_pod(self._namespace, pod)

    def _create_observer_if_needed(self, stream, observer):
        stream_name = stream['metadata']['name']
        observer_t = observer['t']
        component_name = f'observer-{observer_t}'

        if not self._component_exists(component_name=component_name, stream_name=stream_name):
            log.info(f"{observer_t} will be created for {stream_name}")
            self._create_observer(stream, observer)
        else:
            log.debug(f"{observer_t} already exists for {stream_name}")

    def _create_observer(self, stream, observer):
        env_vars = self._extract_vars_from_definition(observer)
        pod = ComponentDefinitionBuilder(stream_meta=stream['metadata'],
                                         component_name=f"observer-{observer['t']}",
                                         env=self._env,
                                         env_vars=env_vars).build()
        self._core_api.create_namespaced_pod(self._namespace, pod)

    def _extract_vars_from_definition(self, component):
        key = lambda k: f'VIDOG_{_to_upper_snake(k)}'
        return {key(k): v for k, v in component.items() if k != 't'}


def _to_upper_snake(s):
    s = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', s)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s).upper()

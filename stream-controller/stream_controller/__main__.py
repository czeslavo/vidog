import os
from kubernetes import client, config

from stream_controller.stream_watcher import StreamWatcher
from stream_controller.env import Environment

def main():
    '''Main entry point for Stream Controller.
   It will create CRDs if needed and start watching Stream resources.
   '''
    try:
        config.load_kube_config()
    except FileNotFoundError:
        config.load_incluster_config()

    api_client = client.ApiClient()

    env = Environment(
        VIDOG_NAMESPACE=os.getenv('VIDOG_NAMESPACE', 'default'),
        VIDOG_CONSUL_ENABLED=os.getenv('VIDOG_CONSUL_ENABLED'),
        VIDOG_CONSUL_DOMAIN=os.getenv('VIDOG_CONSUL_DOMAIN', 'consul'),
        VIDOG_ANALYSER_IMAGE=os.getenv('VIDOG_ANALYSER_IMAGE', 'czeslavo/vidog-analyser-motion-detector:latest'),
    )

    stream_watcher = StreamWatcher(api_client=api_client, env=env)
    stream_watcher.watch()


if __name__ == '__main__':
    main()
import yaml

from stream_controller.logger import log


class ComponentDefinitionBuilder:
    def __init__(self, stream_meta, component_name, env, env_vars=None):
        self.stream_meta = stream_meta
        self.component_name = component_name
        self._env = env
        self._env_vars = env_vars if env_vars else {}

        with open('/app/definitions/component_pod_template.yaml') as f:
            self._template = yaml.load(f, Loader=yaml.FullLoader)

    def build(self):
        self._set_image()
        self._set_name()
        self._set_owner_reference()
        self._set_container()
        return self._template

    def _set_image(self):
        self._template['spec']['containers'][0][
            'image'] = f'czeslavo/vidog-{self.component_name}:latest'

    def _set_name(self):
        self._template['metadata']['name'] = build_component_name(
            self.component_name, self.stream_meta['name'])

    def _set_owner_reference(self):
        self._template['metadata']['ownerReferences'] = [
            {
                'apiVersion': 'v1',
                'kind': 'Stream',
                'name': self.stream_meta['name'],
                'uid': self.stream_meta['uid'],
                'controller': True,
            },
        ]

    def _set_container(self):
        stream_name = self.stream_meta['name']
        env = [
            {
                'name': 'STREAM_URL',
                'value': self._rtmp_endpoint(stream_name)
            },
            {
                'name': 'STREAM_NAME',
                'value': stream_name
            },
            {
                'name': 'VIDOG_REDIS_TOPIC',
                'value': f'stream-events-{stream_name}'
            },
        ]

        for k, v in self._env_vars.items():
            env.append({
                'name': k,
                'value': v,
            })

        self._template['spec']['containers'][0]['env'] = env

    def _rtmp_endpoint(self, stream_name):
        if self._env.VIDOG_CONSUL_ENABLED:
            return f'rtmp://{stream_name}.service.{self._env.VIDOG_CONSUL_DOMAIN}/stream/{stream_name}'

        return f'rtmp://vidog-stream-receiver-cluster/stream/{stream_name}'


def build_component_name(component_name, stream_name):
    return '-'.join((component_name, stream_name))

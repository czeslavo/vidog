import logging
import os 


log = None


def init_logger():
    global log

    logging.basicConfig(format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    log = logging.getLogger('stream-controller')
    log.setLevel(get_level())


def get_level():
    level = os.getenv('LOG_LEVEL', 'INFO')
    return {
        'DEBUG': logging.DEBUG,
        'INFO': logging.INFO,
        'WARN': logging.WARN,
        'ERROR': logging.ERROR
    }.get(level) or logging.INFO 


init_logger()
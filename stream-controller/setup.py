from setuptools import setup 

setup(
    name = 'stream-controller',
    version = '0.0.1',
    packages = ['stream_controller'],
    entry_points = {
        'console_scripts': [
            'stream-controller = stream_controller.__main__:main'
        ]
    }
)
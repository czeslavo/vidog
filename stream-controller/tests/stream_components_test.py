from stream_controller.stream_components import ComponentDefinitionBuilder
from stream_controller.env import Environment


def test_component_definition_builder():
    stream_meta = {
        'name': 'test',
        'uid': '123',
    }
    component_name = 'email-notifier'
    env = Environment(VIDOG_CONSUL_ENABLED='1',
                      VIDOG_CONSUL_DOMAIN='domain',
                      VIDOG_ANALYSER_IMAGE='image',
                      VIDOG_NAMESPACE='ns')
    env_vars = {'VIDOG_ENV1': 'env1', 'VIDOG_ENV2': 'env2'}

    out = ComponentDefinitionBuilder(stream_meta,
                                     component_name,
                                     env,
                                     env_vars).build()

    assert len(out['spec']['containers'][0]['env']) == 4

#!/bin/sh

if [ "$#" -ne 4 ]; then
    echo "Usage: ${0} IMAGE TAG DOCKERFILE CONTEXT"
    exit 1
fi

IMAGE="${1}"
TAG="${2}"
DOCKERFILE="${3}"
CONTEXT="${4}"

echo "Building ${IMAGE}:${TAG}..." && \
docker build -t ${IMAGE}:${TAG} -f ${DOCKERFILE} ${CONTEXT} && \
echo "Publishing..."
docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}" && \
docker push ${IMAGE}:${TAG}

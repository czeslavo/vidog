#!/bin/sh

if [ "$#" -ne 3 ]; then
    echo "Usage: ${0} IMAGE TAG_FROM TAG_TO"
    exit 1
fi

IMAGE="${1}"
TAG_FROM="${2}"
TAG_TO="${3}"

echo "Publishing ${IMAGE}:${TAG_TO}..." && \
docker pull ${IMAGE}:${TAG_FROM} && \
docker tag ${IMAGE}:${TAG_FROM} ${IMAGE}:${TAG_TO} && \
docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}" && \
docker push ${IMAGE}:${TAG_TO}

# Things to remember when setting up Jenkins 

1. Add gitlab credentials (i.e. czeslavo-gitlab-pass)
2. Create PVC before spinning up Jenkins deployment
3. For building docker images on slaves, there's need to mount docker socket from host machine
4. Set proper service account for slaves if they need to use Kubernetes API 
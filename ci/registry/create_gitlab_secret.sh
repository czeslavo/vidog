#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: ${0} password [namespace:default]"
    exit 1
fi

kubectl create secret docker-registry gitlab-reg-cred \
    --docker-server=https://registry.gitlab.com \
    --docker-username=czeslavo@gmail.com \
    --docker-password=${1} \
    --docker-email=czeslavo@gmail.com \
    --namespace ${2:-default}
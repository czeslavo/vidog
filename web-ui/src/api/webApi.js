import { getApiBasePath } from './paths';
import axios from 'axios';

class WebApi {
  listStreams() {
      return axios.get(getApiBasePath() + '/streams');
  }

  createStream(stream) {
    return axios.post(getApiBasePath() + '/streams', stream);
  }

  getStream(streamName) {
    return axios.get(getApiBasePath() + `/streams/${streamName}`);
  }

  deleteStream(streamName) {
    return axios.delete(getApiBasePath() + `/streams/${streamName}`);
  }

  createEventsStream(streamName) {
    return new EventSource(getApiBasePath() + `/streams/${streamName}/events`);
  }

  streamLogsUrl(streamName) {
    if (process.env.NODE_ENV !== 'production')
      return 'https://gist.githubusercontent.com/helfi92/96d4444aa0ed46c5f9060a789d316100/raw/ba0d30a9877ea5cc23c7afcd44505dbc2bab1538/typical-live_backing.log';
    else
      return getApiBasePath() + `/streams/${streamName}/logs`;
  }
}

export default WebApi;
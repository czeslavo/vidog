export function getApiBasePath() {
    if (process.env.NODE_ENV.trim() === "production") {
        return '/api';
    } else {
        return 'https://virtserver.swaggerhub.com/czeslavo/vidog-web-api/1.0.5';
    }
}

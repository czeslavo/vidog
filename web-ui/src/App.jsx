import React, { Component } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';

import Routes from './Routes'
import NavBar from './components/navbar';
import Footer from './components/footer';

class App extends Component {

  render() {
    return (
      <Container>
        <Router>
          <Row>
            <Col>
              <NavBar />
            </Col>
          </Row>
          <Row>
            <Col>
              <Routes />
            </Col>
          </Row>
          <Row style={{paddingTop: 20}}>
            <Col>
              <Footer />
            </Col>
          </Row>
        </Router>
      </Container>
    );
  }
}

export default App;

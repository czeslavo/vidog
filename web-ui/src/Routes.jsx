import React from 'react';
import { Route, Switch } from 'react-router-dom';

import AddStream from './components/addStream';
import ShowStream from './components/showStream';
import ShowStreams from './components/showStreams';

export default () =>
        <Switch>
            <Route path="/" exact component={AddStream} />
            <Route path="/addStream" exact component={AddStream} />
            <Route path="/showStreams" exact component={ShowStreams} />
            <Route path="/watchStream/:streamName" exact component={ShowStream} />
        </Switch>
export default function env(varname) {
    return getFromRuntimeEnv(varname) || getFromBuildEnv(varname) || "";
}

function getFromRuntimeEnv(varname) {
    if ('_env' in window) {
        const varFromGlobal = window._env[varname];
        if (typeof varFromGlobal !== 'undefined') {
            return varFromGlobal
        }
    }

    return null
}

function getFromBuildEnv(varname) {
    return process.env[varname];
}
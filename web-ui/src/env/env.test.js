import env from './env';
import assert from 'assert';

it('gets env from global', () => {
    window._env = {varname: 'varvalue_global'}
    assert(env('varname') == 'varvalue_global')
    delete(window._env)
});

it('gets env from build', () => {
    process.env.varname = 'varvalue_build'
    assert.equal(env('varname'), 'varvalue_build')
    delete(process.env.varname);
})

it('gets env from global as first', () => {
    window._env = {varname: 'varvalue_global'}
    process.env.varname = 'varvalue_build'
    assert.equal(env('varname'), 'varvalue_global')
})

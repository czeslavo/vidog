import React, { Component } from 'react';
import { Container, Spinner } from 'react-bootstrap';

import AddStreamForm from './addStreamForm';
import WebApi from '../api/webApi';

const State = {
    BEFORE_ADD: 'before_add',
    ADDING: 'adding',
    SUCCESS_ADDED: 'sucess_added'
}

class AddStream extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            state: State.BEFORE_ADD
        }

        this.api = new WebApi();
    }

    onAdd = (values) => {
        console.log('OnAdd called', values);
        this.setState({state: State.ADDING});
        var apiStream = {
            name: values.streamName,
            analysers: [],
            observers: []
        }

        Object.keys(values.analysers).forEach((key, index) => {
            const shouldBeUsed = values.analysers[key];
            if (shouldBeUsed) {
                apiStream.analysers.push(key)
            }
        });

        if (values.emailNotifierEnabled) {
            apiStream.observers.push({
                t: 'email-notifier',
                emailTo: values.emailNotifierEmail,
                redisEventKind: values.emailNotifierEventKind
            })
        }

        this.api.createStream(apiStream)
            .then(resp => {
                console.log("Received resp", resp);
                this.setState({state: State.SUCCESS_ADDED});
            })
            .catch(err => {
                console.log("Caught error", err)
                this.setState({state: State.BEFORE_ADD});
            });
    }

    render() {
        return (
            <Container>
                <h4>Create a stream</h4>
                {this.state.state === State.BEFORE_ADD && <AddStreamForm onAdd={this.onAdd} />}
                {this.state.state === State.ADDING && <span><Spinner animation="grow" /><p>Creating...</p></span>}
                {this.state.state === State.SUCCESS_ADDED && <p>Successfuly added a stream</p>}
            </Container>
        )
    }
}

export default AddStream;
import React from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Formik } from 'formik';
import * as Yup from 'yup';

const StreamSchema = Yup.object().shape({
    streamName: Yup.string()
        .min(2, 'Too short')
        .max(20, 'Too long')
        .lowercase('Must be lowercase')
        .matches(/^([a-z\d-]+$)/, 'Must only contain letters, digits or -, i.e. my-name3')
        .required('Required')
        .strict(),
    analysers: Yup.object().shape({
        "motion-detector": Yup.boolean(),
        "face-detector": Yup.boolean()
    }),
    emailNotifierEnabled: Yup.boolean(),
    emailNotifierEmail: Yup.string().email().when('emailNotifierEnabled', (o, schema) => o === true ? schema.required() : schema),
    emailNotifierEventKind: Yup.string().when('emailNotifierEnabled', (o, schema) => o === true ? schema.required() : schema)
})

export default ({ onAdd }) => {
    return (
        <Formik
            initialValues={{
                streamName: '',
                analysers: {
                    "motion-detector": false,
                    "face-detector": false
                },
                emailNotifierEnabled: false,
                emailNotifierEmail: '',
                emailNotifierEventKind: 'motion-detected'
            }}
            validationSchema={StreamSchema}
            onSubmit={(values, actions) => {
                console.log("On submit called");
                onAdd(values);
                actions.setSubmitting(false);
            }}
            render={({
                values,
                errors,
                status,
                touched,
                handleBlur,
                handleChange,
                handleSubmit,
                isSubmitting,
            }) => (
                    <Form onSubmit={handleSubmit}>
                        <Form.Group controlId="streamName">
                            <Form.Label>Stream name</Form.Label>
                            <Form.Control
                                type="text"
                                name="streamName"
                                value={values.streamName}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isInvalid={!!errors.streamName}
                            />
                            <Form.Control.Feedback type="invalid">
                                {errors.streamName}
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Row>
                            <Col>
                                <Form.Group controlId="analysers">
                                    <Form.Label>Analysers</Form.Label>
                                    <Form.Check type="checkbox"
                                        id="analysers.motion-detector"
                                        label="motion-detector"
                                        onChange={handleChange}
                                        onBlur={handleBlur} />
                                    <Form.Check type="checkbox"
                                        id="analysers.face-detector"
                                        label="face-detector"
                                        onChange={handleChange}
                                        onBlur={handleBlur} />
                                    <Form.Text className="text-muted">
                                        Pick analysers for your stream
                                </Form.Text>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group controlId="emailNotifier">
                                    <Form.Label>E-mail notifier</Form.Label>
                                    <Form.Check type="checkbox"
                                        id="emailNotifierEnabled"
                                        size="sm"
                                        label="Enabled"
                                        onChange={handleChange}
                                        onBlur={handleBlur} />
                                    <div hidden={!values.emailNotifierEnabled}>
                                    <Form.Label>Target e-mail</Form.Label>
                                    <Form.Control
                                        type="text"
                                        size="sm"
                                        name="emailNotifierEmail"
                                        value={values.emailNotifierEmail}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={!!errors.emailNotifierEmail}
                                    />
                                    <Form.Label>Event kind to observe</Form.Label>
                                    <Form.Control as="select"
                                        name="emailNotifierEventKind"
                                        size="sm"
                                        value={values.emailNotifierEventKind}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        isInvalid={!!errors.emailNotifierEventKind}
                                    >
                                        <option>motion-detected</option>
                                        <option>face-detected</option>
                                    </Form.Control>
                                    </div>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Button variant="primary" type="submit" disabled={isSubmitting}>
                            Create
                            </Button>
                    </Form>
                )} />
    );
};
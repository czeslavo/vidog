import React, { Component } from 'react';
import Hls from 'hls.js';

class HLSSource extends Component {
    state = {}

    constructor(props, context) {
        super(props, context);
        const hlsConfig = {
            liveDurationInfinity: true,
            enableWorker: false // FIXIT: https://github.com/video-dev/hls.js/issues/2064
        }
        this.hls = new Hls(hlsConfig);
    }

    componentDidMount() {
        this.createHls();
    }

    createHls() {
        const { src, video } = this.props;
        if (Hls.isSupported()) {
            console.log('Hls is supported');
            this.hls.loadSource(src);
            this.hls.attachMedia(video);
            this.hls.on(Hls.Events.MANIFEST_PARSED, (event, data) => {
                video.play();
                this.props.onParsed(data);
            });

            this.hls.on(Hls.Events.ERROR, (event, data) => {
                if (data.fatal) {
                    this.handleFatalError(data);
                }
            })
        } else {
            console.log('Hls not supported');
        }
    }

    handleFatalError(data) {
        switch (data.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:
                console.log("Fatal network error in Hls, trying to recover");
                const recoveryAction = () => {
                    console.log("Recovery actions itself", this.hls);
                    //this.hls.stopLoad();
                    this.hls.loadSource(this.props.src);
                };
                this.props.onNetworkError(data, recoveryAction);
                break;
            case Hls.ErrorTypes.MEDIA_ERROR:
                console.log("Fatal media error in Hls, trying to recover");
                this.hls.recoverMediaError();
                break;
            default:
                console.log("Other fatal error, destroying hls source")
                this.hls.destroy();
                break;
        }
    }

    componentWillUnmount() {
        if (this.hls) {
            this.hls.destroy();
        }
    }

    render() {
        return (
            <source
                src={this.props.src}
                type={this.props.type || 'application/x-mpegURL'}
            />
        );
    }
}

export default HLSSource;
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import WebApi from '../api/webApi';

export default class ShowStreams extends Component {

    constructor(props) {
        super(props);
        this.state = {
            streams: null,
            loadingStatus: 'Loading...'
        }

        this.api = new WebApi();
    }

    componentDidMount() {
        this.api.listStreams()
            .then(resp => {
                console.log("Received resp", resp);
                this.setState({ streams: resp.data });
            })
            .catch(err => {
                console.log("Caught error", err);
                this.setState({ loadingStatus: `Error while getting streams: ${err.message}` })
            });

    }

    render() {
        const { streams } = this.state;

        if (this.state.streams !== null) {
            return (
                <Container>
                <h4>Show streams</h4>
                <ul>
                    {streams.map((stream, i) => {
                        return (
                            <li key={i}>
                                <Link to={`/watchStream/${stream.name}`}>{stream.name}</Link>
                            </li>
                        );
                    })}
                </ul>
                </Container>
            );
        } else {
            return (<p>{this.state.loadingStatus}</p>);
        }

    }
}
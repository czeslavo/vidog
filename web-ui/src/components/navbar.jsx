import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class NavBar extends Component {
    render() {
        return (
            <Navbar sticky="bottom">
                <LinkContainer to="/addStream">
                    <Navbar.Brand>vidog</Navbar.Brand>
                </LinkContainer>
                <Navbar.Toggle />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <LinkContainer to="/addStream">
                            <Nav.Link>Create a stream</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/showStreams">
                            <Nav.Link>Show streams</Nav.Link>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>

            </Navbar>
        );
    }
}

export default NavBar;
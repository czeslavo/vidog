import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';

const columns = [{
    dataField: 'time',
    text: 'When',
    sort: true,
    formatter: cell => {
        cell = new Date(cell * 1000).toLocaleString()
        return cell
    }
}, {
    dataField: 'kind',
    text: 'Event type',
    filter: textFilter()
}, {
    dataField: 'message',
    text: 'Message',
    filter: textFilter()
}]

function noDataIndication() {
    return "Waiting for events..."
}

function indexEvents(events) {
    return events.map((e, i) => {
        e.id = i;
        return e;
    })
}

export default ({ events }) => {
    return (
        <BootstrapTable keyField='id'
                        data={indexEvents(events)}
                        columns={columns}
                        pagination={paginationFactory()}
                        filter={filterFactory()}
                        noDataIndication={noDataIndication}
                        striped
                        hover
                        condensed />
    );
}

import React from 'react';
import ReactDOM from 'react-dom';
import GoLiveButton from './goLiveButton';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GoLiveButton streamName='test-stream' loadingVideo={true}/>, div);
});

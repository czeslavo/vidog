import React from 'react';
import { Badge, Popover, OverlayTrigger, Button } from 'react-bootstrap';
import env from '../env/env';

export default ({ streamName, loadingVideo }) => {
    if (loadingVideo) {
        return (
            <OverlayTrigger trigger="click" placement="right" overlay={startStreamingTip(streamName)}>
                <Button variant="primary" size="sm">Go live</Button>
            </OverlayTrigger >
        );
    } else {
        return null;
    }
};

function startStreamingTip(streamName) {
    const rtmpEndpoint = env('VIDOG_RTMP_ENDPOINT') + "/" + streamName;
    return (
        <Popover id="popover-basic" title="Stream your video with RTMP" >
            In order to go <strong> live</strong > you should direct your video to RTMP endpoint: <br />
            <Badge variant="dark">{rtmpEndpoint}</Badge> <br />
            On Android phone it's recommended to use <strong>Larix Broadcaster</strong> application.
    </Popover >
    );
}
import React, { Component } from 'react';
import { Player } from 'video-react';
import { Container, Row, Col, Card, Badge, Button } from 'react-bootstrap';
import '../../node_modules/video-react/dist/video-react.css';
import HLSSource from './hlsSource';
import { LazyLog } from 'react-lazylog/es5';
import 'react-lazylog/es5/index.css';

import WebApi from '../api/webApi';
import EventsTable from './eventsTable';
import GoLiveButton from './goLiveButton';

class Stream extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            stream: null,
            loadingStatus: 'Loading...',
            manifest: null,
            networkErrorTimerId: null,
            loadingVideo: true,
            events: []
        }

        this.api = new WebApi();
        this.eventsSource = null;
    }

    componentDidMount() {
        this.api.getStream(this.props.streamName)
            .then(resp => {
                console.log("Received resp", resp);
                this.setState({ stream: resp.data });
                this.eventsSource = this.api.createEventsStream(this.state.stream.name);
                this.eventsSource.onmessage = (e) => { this.handleEvent(e) };
            })
            .catch(err => {
                console.log("Caught error", err);
                this.setState({ loadingStatus: `Error while getting stream: ${err}` })
            });
    }

    onParsed = (data) => {
        if (this.state.networkErrorTimerId) {
            clearTimeout(this.state.networkErrorTimerId);
        }

        this.setState({
            loadingVideo: false,
            networkErrorTimerId: null
        });
        console.log('OnParsed called', data);
    }

    handleNetworkError = (_, recoveryAction) => {
        const _2s = 2000;
        const timerId = setTimeout(() => {
            console.log("Calling recovery action for network error");
            recoveryAction();
        }, _2s);
        this.setState({ networkErrorTimerId: timerId });
        this.setState({ loadingVideo: true })
    }

    handleEvent = (event) => {
        let e = JSON.parse(event.data)
        let events = this.state.events.slice()
        e.id = events.length
        e.date = (new Date(e.date * 1000).toLocaleTimeString())
        events.push(e)
        this.setState({
            events: events
        });
    }

    componentWillUnmount() {
        const { networkErrorTimerId } = this.state;
        if (networkErrorTimerId) {
            console.log("Clearing timer for network error recovery");
            clearTimeout(networkErrorTimerId);
            this.setState({ networkErrorTimerId: null });
        }
    }

    handleDelete = (streamName) => {
        return () => {
            this.api.deleteStream(streamName)
                .then(() => {
                    console.log("Deleted stream");
                    window.location.replace("/showStreams")
                })
                .catch(err => {
                    console.log("Error deleteing stream", err);
                });
        }
    }


    render() {
        const { stream } = this.state;

        if (stream !== null) {
            return (
                <Container>
                    <Row>
                        <Col>
                            {this.renderLeft(stream)}
                        </Col>
                        <Col xs={6}>
                            {this.renderRight(stream)}
                        </Col>
                    </Row>
                </Container>
            );
        } else {
            return (<p>{this.state.loadingStatus}</p>);
        }
    }

    renderLeft(stream) {
        return (
            <Card>
                <Card.Body>
                    {this.renderStreamTitle(stream)}
                    {this.renderStreamVideoPlayer(stream)}
                    {this.renderStreamControls(stream)}
                </Card.Body>
            </Card>);
    }

    renderStreamTitle(stream) {
        return (
            <Card.Title>{stream.name} {
                <Badge variant={this.state.loadingVideo ? 'danger' : 'success'}>
                    {this.state.loadingVideo ? 'offline' : 'online'}
                </Badge>
            }</Card.Title>
        );
    }

    renderStreamVideoPlayer(stream) {
        return (
            <Player>
                <HLSSource
                    isVideoChild
                    src={stream.video_url}
                    onParsed={this.onParsed}
                    onNetworkError={this.handleNetworkError}
                />
            </Player>
        );
    }

    renderStreamControls(stream) {
        return (
            <Container style={{ paddingTop: 10, paddingLeft: 0 }}>
                <GoLiveButton streamName={stream.name} loadingVideo={this.state.loadingVideo} />
                <Button onClick={this.handleDelete(stream.name)} variant="warning" size="sm">Delete</Button>
            </Container>
        );
    }
    renderRight(stream) {
        return (
            <Card>
                <Card.Body>
                    <Card.Title>Events</Card.Title>
                    {this.renderStreamEvents(stream)}
                </Card.Body>
            </Card>
        );
    }

    renderStreamEvents(stream) {
        return (
            <EventsTable events={this.state.events} />
        )
    }

    renderStreamLogs(stream) {
        return (
            <LazyLog
                follow={true}
                stream
                height={400}
                url={this.api.streamLogsUrl(stream.name)} />
        );
    }
}

export default Stream;
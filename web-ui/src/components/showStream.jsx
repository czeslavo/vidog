import React from 'react';

import Stream from './stream';

export default ({ match }) => {
    return (
        <Stream streamName={match.params.streamName}/>
    );
}

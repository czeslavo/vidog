#!/bin/bash

export STREAM_RECEIVER_SERVICE="vidog-stream-receiver-cluster"

if [ "$VIDOG_CONSUL_ENABLED" == "1" ]; then
    echo "Consul enabled"
    export STREAM_RECEIVER_SERVICE='$1.service.'"${VIDOG_CONSUL_DOMAIN}"
fi

echo "VIDOG_CONSUL_ENABLED $VIDOG_CONSUL_ENABLED"

envsubst '${STREAM_RECEIVER_SERVICE}' < /etc/nginx/nginx.conf > /etc/nginx/_nginx.conf
mv /etc/nginx/_nginx.conf /etc/nginx/nginx.conf

echo "Using config: $(cat /etc/nginx/nginx.conf)"

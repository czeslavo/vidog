#!/bin/bash

/hooks/inject_nginx_env.sh
/hooks/inject_js_env.sh

exec "$@"
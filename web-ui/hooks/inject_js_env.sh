#!/bin/bash

config=/var/www/config.js

rm -rf ${config}
touch ${config}

echo "window._env = {" >> ${config}

while read -r line || [[ -n "$line" ]];
do
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  echo "  $varname: \"$varvalue\"," >> ${config}
done < <(env | grep 'VIDOG_*')

echo "}" >> ${config}

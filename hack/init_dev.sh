#!/bin/bash

createClusterIfNotRunning() {
    running="$(k3d ls | grep running)"
    if [ -z "${running}" ]; then
        echo "Creating k3s cluster..."
        k3d create
        sleep 5
    fi
}

exportKubeconfig() {
    export KUBECONFIG="$(k3d get-kubeconfig)"
    echo "Exported KUBECONFIG=$KUBECONFIG"
}

createClusterIfNotRunning
exportKubeconfig
skaffold dev
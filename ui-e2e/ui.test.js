const assert = require('assert')
const puppeteer = require('puppeteer')

let browser
let page

const targetUrl = `https://${process.env.TARGET}`

before(async() => {
  browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage'
    ]
  })

  const browserVersion = await browser.version()
  console.log(`Started ${browserVersion}`)
})

beforeEach(async() => {
  page = await browser.newPage()
  await page.goto(targetUrl, {waitUntil: 'load', timeout: 0})
})

afterEach(async() => {
  await page.close()
})

after(async() => {
  await browser.close()
})

createStream = async (stream) => {
    await page.focus('input#streamName')
    await page.keyboard.type(stream)
    await page.$eval('button.btn-primary[type="submit"]', b => b.click())
    await page.waitForFunction('document.querySelector("p").innerText === "Successfuly added a stream"')
}

deleteStream = async (stream) => {
    await page.goto(`${targetUrl}/watchStream/${stream}`, {waitUntil: 'load', timeout: 0})

    const deleteButtonSelector = 'button.btn-warning[type="button"]'
    await page.waitForSelector(deleteButtonSelector)
    await page.$eval(deleteButtonSelector, b => b.click())
}

describe('ui', () => {
  it('creates and deletes streams', async() => {
    const stream = 'test-stream'
    await createStream(stream)
    await deleteStream(stream)
  })
})
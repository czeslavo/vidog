#!/bin/bash

# Register stream host in a service registry

source /hooks/common.sh

STREAM_NAME="${1}"

log "Trace: register_stream.sh called with args: $@"
log "Environment: $(env)"
log "STREAM_NAME=${STREAM_NAME} POD_IP=${POD_IP}"

if [[ -v VIDOG_CONSUL_ENABLED ]]; then
    log "Registering ${STREAM_NAME} in Consul: ${POD_IP}"
    request='{"node": "dns", "address": "'"${POD_IP}"'", "service": {"service": "'"${STREAM_NAME}"'", "address": "'"${POD_IP}"'"}}'
    log "Request: ${request}"

    curl -XPUT -H "Content-Type: application/json" http://consul.service.${VIDOG_CONSUL_DOMAIN}:8500/v1/catalog/register -d "${request}"
fi

# TODO: set status of the stream with kubectl

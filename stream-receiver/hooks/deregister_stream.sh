#!/bin/bash

# Deregister stream host from a service registry

source /hooks/common.sh

STREAM_NAME="${1}"

log "Trace: deregister_stream.sh called with args: $@"
log "Environment: $(env)"
log "STREAM_NAME=${STREAM_NAME}"

if [[ -v VIDOG_CONSUL_ENABLED ]]; then
    log "Deregistering ${STREAM_NAME} from Consul: ${POD_IP}"
    request='{"node": "dns", "serviceId": "'"${STREAM_NAME}"'"}'
    log "Request: ${request}"

    curl -XPUT -H "Content-Type: application/json" http://consul.service.${VIDOG_CONSUL_DOMAIN}:8500/v1/catalog/deregister -d "${request}"
fi
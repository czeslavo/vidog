import face_recognition
import cv2
import numpy as np
import time
import os
import sys
import json
from datetime import datetime, timedelta
from threading import Thread

import imutils
import redis

stream_url = os.getenv('STREAM_URL', '/app/sample_video.avi')
preview = os.getenv('PREVIEW', '0') == '1'
stream_name = os.getenv('STREAM_NAME', 'none')
event_period = int(os.getenv('EVENT_PERIOD', '5')) # in sec

def get_redis():
    return redis.Redis(host='redis', db=0)

def log(*args):
    print(*args)
    sys.stdout.flush()

class VideoStream:
    def __init__(self, url):
        self.stream = cv2.VideoCapture(url)
        self.fps = self.stream.get(cv2.CAP_PROP_FPS)
        log("FPS", self.fps)
        self.ok, self.frame = self.stream.read()

    def read(self):
        for _ in range(15):
            self.stream.grab()

        return self.stream.read()

    def isOpened(self):
        return self.stream.isOpened()


    def open(self, url):
        return self.stream.open(url)

    def release(self):
        return self.stream.release()


video_capture = VideoStream(stream_url)
re = get_redis()
last_detection = None

if __name__ == '__main__':
    while True:
        if not video_capture.isOpened():
            if not video_capture.open(stream_url):
                log('Cannot open stream, waiting (5s)...')
                time.sleep(5)
                continue

        ok, frame = video_capture.read()
        if frame is None or not ok:
            log('Stream ended')
            video_capture.release()
            continue

        if last_detection and (datetime.utcnow() - last_detection) < timedelta(seconds=event_period):
            continue

        small_frame = imutils.resize(frame, width=400)
        rgb_small_frame = small_frame[:, :, ::-1]
        face_locations = face_recognition.face_locations(rgb_small_frame)

        if face_locations:
            log("Face detected")
            last_detection = now = datetime.utcnow()
            snapshot = f'{stream_name}_{int(now.timestamp())}.png'
            cv2.imwrite(f'/www/storage/{snapshot}', frame)
            event = {
                'kind': 'face-detected',
                'message': 'Took a snapshot',
                'time': now.timestamp(),
                'data': {
                    'snapshotPath': f'/storage/{snapshot}'
                }
            }
            re.publish('stream-events-{}'.format(stream_name), json.dumps(event))

        if preview:
            cv2.imshow('Motion detector', small_frame)

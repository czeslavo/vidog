''' Motion detector detects motion basing on difference between first seen
frame and the current one.

Pass parameters via environment variables:
    STREAM_URL (default: /app/sample_video.avi): stream URL or video file path
    PREVIEW (default: 0): show window with preview
    MIN_AREA (default: 1000): threshold for motion detection
    INTERVAL (default: 5s): interval in which motion will be detected (in seconds)
    STREAM_NAME (default: none): name of the stream which is analysed
'''

import time
import os
import sys
import json
from datetime import datetime, timedelta

import cv2
import imutils
import redis

stream_url = os.getenv('STREAM_URL', '/app/sample_video.avi')
preview = os.getenv('PREVIEW', '0') == '1'
min_area = int(os.getenv('MIN_AREA', '1000'))
interval = int(os.getenv('INTERVAL', '5'))
stream_name = os.getenv('STREAM_NAME', 'none')


detected = [False, datetime.now()]

def get_redis():
    return redis.Redis(host='redis', db=0)

def log(*args):
    print(*args)
    sys.stdout.flush()

def main():
    log(os.environ)

    log("Starting motion-detector: {}".format(stream_url))
    stream = cv2.VideoCapture(stream_url)
    first_frame = None
    re = get_redis()

    while True:
        if not stream.isOpened():
            if not stream.open(stream_url):
                log('Cannot open stream, waiting (5s)...')
                time.sleep(5)
                continue

        got, frame = stream.read()
        if frame is None or not got:
            log('Stream ended')
            stream.release()
            first_frame = None
            continue

        frame = imutils.resize(frame, width=500)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        if first_frame is None:
            first_frame = gray
            continue

        if detected[0] and (detected[1] + timedelta(seconds=interval)) < datetime.now():
            first_frame = gray
            detected[0] = False  # reset detection

        frame_delta = cv2.absdiff(first_frame, gray)
        thresh = cv2.threshold(frame_delta, 25, 255, cv2.THRESH_BINARY)[1]

        thresh = cv2.dilate(thresh, None, iterations=2)
        cnts = cv2.findContours(
            thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)

        if any(cv2.contourArea(c) > min_area for c in cnts) and not detected[0]:
            # motion detected
            now = datetime.utcnow()
            log(now, "Motion detected!")
            event = {
                'kind': 'motion-detected',
                'time': now.timestamp(),
                'message': 'something moved'
            }
            re.publish('stream-events-{}'.format(stream_name), json.dumps(event))

            detected[0] = True
            detected[1] = now

        if preview:
            cv2.imshow('Motion detector', frame)

        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break

    cv2.destroyAllWindows()


if __name__ == '__main__':
    main()

Continous Integration and Continous Deployment
==============================================

Continous integration pipeline is defined in .gitlab-ci.yaml.
It builds, tests and publish Docker images to the registry.

The next part is continous deployment stage.
I'm using weave-flux operator for this which enables as-called GitOps way of
handling deployment.

Installing weave-flux
---------------------

To install it I used helm package manager. Firstly, weaveworks chart repo
had to be added to my tiller instance:

.. code-block:: bash

    helm repo add weaveworks https://weaveworks.github.io/flux

Then I installed flux operator in the cluster:

.. code-block:: bash

    helm install --name vidog-flux \
        --set rbac.create=true \
        --set git.url=git@gitlab.com:czeslavo/vidog-deployment.git \
        --namespace flux \
        weaveworks/flux

As it's already included in the command above, I created a repository under
gitlab.com/czeslavo/vidog-deployment which is used for the manifests storage.

Flux needs write access to the repository since it updates deployment's images
versions when it's needed. Therefore I needed to add a SSH key of the Flux controller
to the repository's deploy keys.

The key was obtained using following command:

.. code-block:: bash

    kubectl -n flux logs deployment/vidog-flux | grep identity.pub | cut -d '"' -f2
Consul
======

`Consul <https://www.consul.io/>`_ is used as a DNS server for internal stub domains.

The need for such domains comes from a problem described below.

stream-receiver statefulness problem
------------------------------------

Let's describe a simple scenario:

Prerequisites:
    1. There are multiple instances of stream-receiver in the cluster: instance-A and instance-B

Steps:
    1. User starts streaming over RTMP and gets directed to one of the instances, let's say instance-A
    2. Users configures the stream to have an analyser
    3. Analyser spawns and wants to get access to the stream, so it queries internal stream-receiver service and gets directed to... instance-B
    4. Because user got connected with instance-A and is streaming video there, analyser won't find the stream on instance-B

Result:
    Analyser isn't able to perform its job since it has no reliable access to RTMP stream.

Our problem is statefulness of stream-receivers. To solve it, we need to add
a layer of indirection, in our case programmable DNS server, such as Consul.

Solution
--------

Prerequisites:
    1. Configure in-cluster Consul instance with a domain "vidog-consul"

Steps:
    1. Users starts streaming over RTMP and gets directed to one of the instances, let's say instance-A
    2. stream-receiver launches a registration hook on publish event which sends an HTTP registration request to Consul
    3. Consul registers stream with a user defined name and stream-receiver's Pod IP address in its registry and makes it queryable via DNS
    4. User configures the stream to have an analyser
    5. Analyser spawns and queries Consul DNS for streamName.service.vidog-consul getting successfuly connected to instance-A, which has the user's stream

Result:
    Analyser has a reliable access to the stream and is able to perform its job.

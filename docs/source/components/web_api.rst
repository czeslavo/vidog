Web API
-------

Web API is used by Web UI to manipulate streams in the system.

Web API specification
=====================

.. openapi:: ./web_api_spec.json

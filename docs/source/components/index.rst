Components
==========

.. toctree::
    :maxdepth: 1

    stream_controller
    web_api
    consul


Below diagram is an overview of the system components and their internal and external interfaces.

.. image:: https://www.plantuml.com/plantuml/png/0/TPDDRi8m48NtFeN5kia1K5LH8IIgjXA2x10B9nb8X6Ex_e6YgdVlU9AK14HMZlszcJSlpinvONre99_hncW5oZl6QkN1xaK1V9bbgytWVRBys4ouSBoIRh2zpZvNSTVwnWpfzMAJyTVL6qaiEC-OqYKUp6LmI4tu3-Fu32Jl8GUhm8FZ8_KnxiSAD_nRN2eXoGEHU4W-Ri352OVZNm4244OhHfGHnL4SeYrjWHV_IVJsuhCzGvw4UiVJOD8N6ePFUN3mYA3e475WJmF8UGkYIGgDBhMKO957kXyF1MVXYsfSLPWwoT4agF8EntBb4m-c5FxQXiwUq_HUJiXlEtTlneHSrgu2wx1wgSyAEtGfOWCJf6nnY14c9NwSmck2RNseFD8K9h6kjTvka8DMfNCA8ggcl7jvR88In3isoe_yKCbEOg60kg-9OWzgL79mdjwCHBtwAt8jV3TXDpu6jeNlzgObxeATcjt3wPJszVx2s0ppnb_h3m00
    :alt: Components
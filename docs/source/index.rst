*vidog*
=================================

Vidog is a platform for live video streams analysis in the cloud. It leverages Kubernetes platform and builts abstractions over it.

.. toctree::
    :maxdepth: 2

    components/index
    user_stories
    cicd
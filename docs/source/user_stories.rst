============
User stories
============

W tym dokumencie zapisano wymagania funkcjonalne w postaci user stories. Posegregowano je w grupy związane z wymaganiami systemowowymi.

#. Jako system, chcę posiadać w klastrze agenta, aby móc przy jego pomocy zarządzać strumieniami w klastrze.
	#. Jako agent systemu, chcę tworzyć instancje strumienia wideo, aby możliwe było przesyłanie do nich wideo celem analizy.
	#. Jako agent systemu, chcę konfigurować instancje strumienia wideo, aby możliwe było ustalanie poszczególnych parametrów analizy i udostępniania danego strumienia.
	#. Jako agent systemu, chcę tworzyć procesory strumienia wideo w klastrze na podstawie konfiguracji, aby mogły one analizować poszczególne strumienie.
	#. Jako agent systemu, chcę tworzyć publishery strumienia wideo w klastrze na podstawie konfiguracji, aby uprawnieni użytkownicy mogli je oglądać.
	#. Jako agent systemu, chcę kontrolować stan poszczególnych strumieni w klastrze, aby możliwe było reagowanie na zmiany globalnego stanu.
	#. Jako agent systemu, chcę tworzyć odbiorców strumienia wideo na podstawie aktualnych potrzeb, aby możliwe było obsłużenie większej ilości strumieni.

#. Jako system, chcę posiadać bazę strumieni, aby moje komponenty mogły z niej korzystać.
	#. Jako baza strumieni, chcę posiadać informacje na temat dodanych strumieni, aby udostępniać wiedzę o ich istnieniu.
	#. Jako baza strumieni, chcę posiadać konfigurację poszczególnych strumieni, aby być ich point of authority.

#. Jako system, chcę posiadać odbiorców strumienia wideo, abym mógł przyjmować strumienie z zewnątrz klastra.
	#. Jako odbiorca strumienia wideo, chcę nasłuchiwać nowych połączeń RTMP, abym mógł je udostępniać lokalnie.
	#. Jako odbiorca strumienia wideo, chcę udostępniać strumień RTMP lokalnie, aby możliwy był jego odbiór przez inne komponenty.
	#. Jako odbiorca strumienia wideo, chcę zezwalać na połączenie RTMP tylko autoryzowanym użytkownikom, aby nie było możliwe nieautoryzowane przesyłanie wideo.
	#. Jako odbiorca strumienia wideo, chcę zarejestrować serwis strumienia w lokalnym rejestrze DNS podczas nawiązania połączenia, aby inne komponenty mogły zlokalizować gdzie dany strumień jest odbierany.
	#. Jako odbiorca strumienia wideo, chcę sprawdzić czy podczas nawiązania połączenia strumień z zadaną nazwą istnieje, aby nie odbierać nieutworzonych strumieni.
	#. Jako odbiorca strumienia wideo, chcę sprawdzić czy podczas nawiązania połączenia strumień z zadaną nazwą nie jest już zarejestrowany w lokalnym rejestrze DNS, abym nie duplikował strumieni.
	#. Jako odbiorca strumienia wideo, chcę wyrejestrować serwis strumienia w lokalnym rejestrze DNS podczas zakończenia połączenia, aby możliwe było ponowne nawiązanie połączenia z tą samą nazwą strumienia.

#. Jako system, chcę posiadać procesory strumienia wideo, aby móc analizować poszczególne strumienie wideo.
	#. Jako procesor strumienia wideo, chcę analizować strumień wideo z użyciem parametrów jakimi mnie skonfigurowano, abym mógł wykrywać pożądane zdarzenia w strumieniu.
	#. Jako procesor strumienia wideo, chcę wykrywać zdarzenia i propagować je do kolejki zdarzeń, aby inne komponenty mogły na nie reagować.

#. Jako system, chcę posiadać obserwatorów zdarzeń dla poszczególnych strumieni, aby móc definiować reakcje na poszczególne zdarzenia.
	#. Jako obserwator zdarzeń, chcę odbierać zdarzenia z kolejki zdarzeń, aby móc na nie reagować.
	#. Jako obserwator zdarzeń, chcę reagować na zdarzenia w sposób zadany w konfiguracji, aby zdarzenia mogły wpływać na inne systemy.

#. Jako system, chcę posiadać publisherów strumienia wideo, aby użytkownicy mogli uzyskać do nich dostęp z zewnątrz klastra.
	#. Jako publisher strumienia wideo, chcę udostępniać strumień HLS pod zadanym konfiguracją adresem, aby uprawnieni użytkownicy mogli go oglądać.

#. Jako użytkownik systemu, chcę korzystać z web interfejsu do zarządzania moimi strumieniami, abym w łatwy sposób mógł je tworzyć i konfigurować.
	#. Jako użytkownik web interfejsu, chcę stworzyć strumień wideo, abym mógł przesyłać do niego wideo celem analizy.
	#. Jako użytkownik web interfejsu, chcę konfigurować strumień wideo, abym mógł ustalić poszczególne parametry jego analizy.
	#. Jako użytkownik web interfejsu, chcę usuwać strumienie wideo, aby optymalnie wykorzystywać dostępne mi zasoby.
	#. Jako użytkownik web interfejsu, chcę konfigurować parametry udostępniania wideo, abym mógł decydować o sposobie publikowania go.

package server

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	v1 "gitlab.com/czeslavo/vidog/web-api/pkg/apis/vidog/v1"
	clientV1 "gitlab.com/czeslavo/vidog/web-api/pkg/client/clientset/versioned/typed/vidog/v1"

	"gitlab.com/czeslavo/vidog/web-api/pkg/models"
	senv "gitlab.com/czeslavo/vidog/web-api/pkg/server/env"
	"gitlab.com/czeslavo/vidog/web-api/pkg/server/sse"
	"gitlab.com/czeslavo/vidog/web-api/pkg/server/sse/redis"
)

type Server struct {
	env      senv.Env
	vidogAPI clientV1.VidogV1Interface
	client   kubernetes.Interface
	router   *mux.Router
}

func NewServer(env senv.Env, vidogAPI clientV1.VidogV1Interface, client kubernetes.Interface) (s *Server) {
	s = &Server{
		env:      env,
		vidogAPI: vidogAPI,
		client:   client,
	}

	s.initRouter()
	return
}

func (s *Server) initRouter() {
	s.router = mux.NewRouter().StrictSlash(true)
	routes := []struct {
		method  string
		pattern string
		name    string
		handler http.HandlerFunc
	}{
		{"POST", "/streams", "AddStream", s.addStream},
		{"GET", "/streams/{streamName}", "GetStream", s.getStream},
		{"DELETE", "/streams/{streamName}", "DeleteStream", s.deleteStream},
		{"GET", "/streams", "ListStreams", s.listStreams},
		{"GET", "/streams/{streamName}/logs", "GetStreamRelatedLogs", s.getStreamRelatedLogs},
		{"GET", "/streams/{streamName}/events", "PollStreamEvents", s.pollStreamEvents()},
	}

	for _, route := range routes {
		s.router.
			Methods(route.method).
			Path(route.pattern).
			Name(route.name).
			Handler(route.handler)
	}

	s.router.Use(Logger)
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *Server) addStream(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	decoder := json.NewDecoder(r.Body)
	var newStream models.Stream
	err := decoder.Decode(&newStream)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	stream := &v1.Stream{
		ObjectMeta: metav1.ObjectMeta{
			Name: newStream.Name,
		},
		Status: v1.StreamStatus{
			IsStreaming: false,
		},
		Spec: v1.StreamSpec{
			Name:      newStream.Name,
			Publish:   true,
			Analysers: newStream.Analysers,
			Observers: newStream.Observers,
		},
	}

	_, err = s.vidogAPI.Streams(s.env.Namespace).Create(stream)
	if err != nil {
		if errors.IsAlreadyExists(err) {
			http.Error(w, err.Error(), http.StatusConflict)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	w.WriteHeader(http.StatusCreated)
}

func (s *Server) getStream(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	streamName := mux.Vars(r)["streamName"]

	st, err := s.vidogAPI.Streams(s.env.Namespace).Get(streamName, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			http.Error(w, err.Error(), http.StatusNotFound)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	stream := models.Stream{
		Name:      st.Name,
		VideoUrl:  fmt.Sprintf("/hls/%s.m3u8", st.Name),
		Analysers: st.Spec.Analysers,
		Observers: st.Spec.Observers,
	}

	js, err := json.Marshal(stream)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(js)
}

func (s *Server) deleteStream(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	name := mux.Vars(r)["streamName"]

	err := s.vidogAPI.Streams(s.env.Namespace).Delete(name, &metav1.DeleteOptions{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusGone)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (s *Server) listStreams(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	streams, err := s.vidogAPI.Streams(s.env.Namespace).List(metav1.ListOptions{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	streamsModels := []models.Stream{}
	for _, stream := range streams.Items {
		streamsModels = append(streamsModels, models.Stream{
			Name:     stream.Spec.Name,
			VideoUrl: fmt.Sprintf("/hls/%s.m3u8", stream.Spec.Name),
		})
	}
	js, err := json.Marshal(streamsModels)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(js)
}

func (s *Server) getStreamRelatedLogs(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-Accel-Buffering", "no")

	tail := int64(20)
	limit := r.FormValue("limit")
	if limit != "" {
		i, err := strconv.Atoi(limit)
		if err == nil {
			tail = int64(i)
			log.Printf("Tail=%d", tail)
		}
	}
	logsOptions := corev1.PodLogOptions{
		Follow:    true,
		TailLines: &tail,
	}

	streamName := mux.Vars(r)["streamName"]
	podName := "analyser-motion-detector-" + streamName
	req := s.client.CoreV1().Pods(s.env.Namespace).GetLogs(podName, &logsOptions)

	readCloser, err := req.Stream()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer readCloser.Close()

	fw := flushWriter{w}
	_, err = io.Copy(fw, readCloser)
}

type flushWriter struct {
	w io.Writer
}

func (fw flushWriter) Write(data []byte) (n int, err error) {
	n, err = fw.w.Write(data)
	if f, ok := fw.w.(http.Flusher); ok {
		f.Flush()
	}
	return
}

func (s *Server) pollStreamEvents() http.HandlerFunc {
	log.Printf("Creating redis pool: %s", s.env.RedisURL)
	p := redis.NewPool(s.env.RedisURL)

	hub := sse.NewEventsHub(func(t string) sse.EventSourceInterface {
		return redis.NewRedisSource(t, p)
	})
	sseServer := sse.EventsServer{hub}
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Handling SSE")
		sseServer.HandleHTTP(w, r)
	}
}

package sse

import (
	"log"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

type EventsServer struct {
	Events *EventsHub
}

func (s *EventsServer) HandleHTTP(w http.ResponseWriter, r *http.Request) {
	f, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "streaming unsupported", http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("X-Accel-Buffering", "no")

	streamName := mux.Vars(r)["streamName"]

	topic := fmt.Sprintf("stream-events-%s", streamName)
	ch := s.Events.Topic(topic).Subscribe()
	defer s.Events.Topic(topic).Unsubscribe(ch)

	done := r.Context().Done()
	for {
		select {
		case event := <-ch:
			log.Printf("Pushing to client, %v", event)
			fmt.Fprintf(w, "data: %s\n\n", event)
			f.Flush()
		case <-done:
			return
		}
	}
}
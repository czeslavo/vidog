package sse

import (
	"sync"

)

type EventsHub struct {
	sources map[string]EventSourceInterface
	mtx sync.RWMutex
	sourceFactory SourceFactory
}

type SourceFactory func(t string) EventSourceInterface

func NewEventsHub(f SourceFactory) *EventsHub {
	return &EventsHub{
		make(map[string]EventSourceInterface),
		sync.RWMutex{},
		f,
	}
}

// TODO: remove topics without active subs
func (e *EventsHub) cleanup() {
}

func (e *EventsHub) Topic(t string) EventSourceInterface {
	e.mtx.Lock()
	defer e.mtx.Unlock()
	if s, found := e.sources[t]; found {
		return s
	}
	e.cleanup()

	return e.createSource(t)
}

func (e *EventsHub) createSource(t string) EventSourceInterface {
	// inject factory newEventSource for redis
	s := e.sourceFactory(t)
	e.sources[t] = s
	return s
}
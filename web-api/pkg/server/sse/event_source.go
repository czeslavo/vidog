package sse

import (
	"sync"
	"log"
)

type EventSourceInterface interface {
	HasSubs() bool
	Subscribe() chan []byte
	Unsubscribe(chan []byte)
	Publish([]byte)
}

type EventSource struct {
	topic string
	subs map[chan []byte]bool
	mtx sync.RWMutex
}

func NewEventSource(t string) *EventSource {
	log.Printf("NewEventSource %v", t)
	return &EventSource{t, make(map[chan []byte]bool), sync.RWMutex{}}
}

func (es *EventSource) HasSubs() bool {
	es.mtx.RLock()
	defer es.mtx.RUnlock()

	log.Printf("HasSub %s, %v", es.topic, len(es.subs) > 0)
	return len(es.subs) > 0
}

func (es *EventSource) Subscribe() chan []byte {
	ch := make(chan []byte, 10)

	log.Printf("Subscribing %s", es.topic)
	es.mtx.Lock()
	es.subs[ch] = true
	es.mtx.Unlock()

	return ch
}

func (es *EventSource) Unsubscribe(ch chan []byte) {
	es.mtx.Lock()
	log.Printf("Unsubscribing %s", es.topic)
	close(ch)
	delete(es.subs, ch)
	es.mtx.Unlock()
}

func (es *EventSource) Publish(e []byte) {
	es.mtx.RLock()
	defer es.mtx.RUnlock()
	log.Printf("Sending %v to %d subs", e, len(es.subs))
	for ch := range es.subs {
		ch <- e
	}
}

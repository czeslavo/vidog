package sse_test

import (
	"sync"

	"testing"
	"github.com/stretchr/testify/assert"

	"gitlab.com/czeslavo/vidog/web-api/pkg/server/sse"
)

func TestEventSource(t *testing.T) {
	es := sse.NewEventSource("topic")
	var wg sync.WaitGroup

	ch := es.Subscribe()
	assert.True(t, es.HasSubs())
	wg.Add(1)
	receivedEvents := make([][]byte, 0)
	go func() {
		defer es.Unsubscribe(ch)
		defer wg.Done()

		counter := 0
		for {
			event := <-ch
			receivedEvents = append(receivedEvents, event)

			counter += 1
			if (counter == 3) {
				break
			}
		}
	}()

	events := [][]byte{
		[]byte(`{"StreamEvent", 121.001, "message which is important"}`),
		[]byte(`{"StreamEvent", 121.002, "message which is fancy"}`),
		[]byte(`{"StreamEvent", 251.711, "message which is the last"}`),
	}

	for _, e := range events {
		es.Publish(e)
	}

	wg.Wait()

	assert.Equal(t, events, receivedEvents)
	_, open := (<-ch)
	assert.False(t, open)
	assert.False(t, es.HasSubs())
}

package sse_test

import (
	"sync"

	"testing"
	"github.com/stretchr/testify/assert"

	"gitlab.com/czeslavo/vidog/web-api/pkg/server/sse"
)

func TestEventsHub(t *testing.T) {
	hub := sse.NewEventsHub(func(t string) sse.EventSourceInterface {
		return sse.NewEventSource(t)
	})
	topic := "topic"
	event := []byte(`{"Message", 8000.1, "message content"}`)

	var wgSub, wgDone sync.WaitGroup
	// run 3 goroutines with hub and subscribe to the same topic
	// assure they all receive the same event
	for i := 0; i < 3; i++ {
		wgSub.Add(1)
		wgDone.Add(1)
		go func(h *sse.EventsHub) {
			ch := h.Topic(topic).Subscribe()
			wgSub.Done()

			e := <-ch
			assert.Equal(t, event, e)
			wgDone.Done()
		}(hub)
	}

	// wait for subs
	wgSub.Wait()
	hub.Topic(topic).Publish(event)

	// wait for receive
	wgDone.Wait()
}
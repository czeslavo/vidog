package redis

import (
	"log"

	redigo "github.com/garyburd/redigo/redis"

	"gitlab.com/czeslavo/vidog/web-api/pkg/server/sse"
)

type RedisSource struct {
	*sse.EventSource
	pool Pool
}

type Pool interface {
	Get() redigo.Conn
}

func NewPool(url string) *redigo.Pool {
	return &redigo.Pool{
		Dial: func() (redigo.Conn, error) { return redigo.Dial("tcp", url) },
	}
}

func NewRedisSource(topic string, pool Pool) sse.EventSourceInterface {
	s := &RedisSource{
		EventSource: sse.NewEventSource(topic),
		pool: pool,
	}

	err := s.run(topic)
	if err != nil {
		log.Fatalf("Couldn't create redis source, %v", err)
	}
	return s
}

func (s *RedisSource) run(key string) error {
	conn := s.pool.Get()
	psConn := redigo.PubSubConn{Conn: conn}

	if err := psConn.Subscribe(key); err != nil {
		return err
	}

	log.Printf("SSE: subscribed for %v in redis", key)

	go func() {
		for {
			switch v := psConn.Receive().(type) {
			case redigo.Message:
				s.Publish(v.Data)
			case error:
				log.Printf("Error from redis, %v", v)
				return
			}
		}
	}()

	return nil
}
package redis_test

import (
	"sync"

	"testing"
	"github.com/stretchr/testify/assert"

	redigo "github.com/garyburd/redigo/redis"
	"github.com/rafaeljusto/redigomock"

	"gitlab.com/czeslavo/vidog/web-api/pkg/server/sse/redis"
)

type PoolMock struct {
	c *redigomock.Conn
}

func (p PoolMock) Get() redigo.Conn {
	return p.c
}

func createSubscriptionMessage(data []byte) []interface{} {
    values := []interface{}{}
    values = append(values, interface{}([]byte("message")))
    values = append(values, interface{}([]byte("chanName")))
    values = append(values, interface{}(data))
    return values
}

func sendSubMessage(data string, conn *redigomock.Conn) {
	conn.AddSubscriptionMessage(createSubscriptionMessage([]byte(data)))
	conn.ReceiveNow <- true
}

func TestRedisEventSource(t *testing.T) {
	conn := redigomock.NewConn()
	pool := PoolMock{conn}

	conn.Command("SUBSCRIBE", "topic").Expect([]interface{}{
		interface{}([]byte("subscribe")),
		interface{}([]byte("topic")),
		interface{}([]byte("1")),
	})
	conn.ReceiveWait = true

	es := redis.NewRedisSource("topic", pool)
	conn.ReceiveNow <- true // for subscription

	var wg sync.WaitGroup
	ch := es.Subscribe()
	assert.True(t, es.HasSubs())
	wg.Add(1)
	receivedEvents := make([][]byte, 0)
	go func() {
		defer es.Unsubscribe(ch)
		defer wg.Done()

		counter := 0
		for {
			event := <-ch
			receivedEvents = append(receivedEvents, event)

			counter += 1
			if (counter == 3) {
				break
			}
		}
	}()

	sendSubMessage(`{"time": 124.1, "message": "first", "kind": "motion-detected"}`, conn)
	sendSubMessage(`{"time": 126.1, "message": "second", "kind": "motion-detected"}`, conn)
	sendSubMessage(`{"time": 125.1, "message": "third", "kind": "motion-detected"}`, conn)
	wg.Wait()

	_, open := (<-ch)
	assert.False(t, open)
	assert.False(t, es.HasSubs())
	assert.Equal(t, len(receivedEvents), 3)
}

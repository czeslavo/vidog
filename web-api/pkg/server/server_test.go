package server_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/suite"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	kubefake "k8s.io/client-go/kubernetes/fake"
	kubetesting "k8s.io/client-go/testing"

	v1 "gitlab.com/czeslavo/vidog/web-api/pkg/apis/vidog/v1"
	fake "gitlab.com/czeslavo/vidog/web-api/pkg/client/clientset/versioned/fake"
	"gitlab.com/czeslavo/vidog/web-api/pkg/models"

	"gitlab.com/czeslavo/vidog/web-api/pkg/server"
	"gitlab.com/czeslavo/vidog/web-api/pkg/server/env"
)

type ServerTestSuite struct {
	suite.Suite

	fake     *fake.Clientset
	kubefake *kubefake.Clientset
	sut      *server.Server
	rec      *httptest.ResponseRecorder
}

func (s *ServerTestSuite) SetupTest() {
	s.fake = fake.NewSimpleClientset()
	s.kubefake = kubefake.NewSimpleClientset()
	s.sut = server.NewServer(env.Env{
		Namespace: "testing",
		RedisURL:  "redis:6379",
	}, s.fake.VidogV1(), s.kubefake)
	s.rec = httptest.NewRecorder()
}

func (s *ServerTestSuite) TestAddStream() {
	body := []byte(`{
		"name": "test",
		"analysers": [
			"motion-detector"
		],
		"observers": [
			{
				"t": "email-notifier",
				"eventKind": "motion-detected",
				"email": "mail@example.com"
			}
		]
	}`)
	req, err := http.NewRequest("POST", "/streams", bytes.NewBuffer(body))
	s.NoError(err)

	s.sut.ServeHTTP(s.rec, req)

	s.Equal(http.StatusCreated, s.rec.Code)
	stream, err := s.fake.VidogV1().Streams("testing").Get("test", metav1.GetOptions{})
	s.NoError(err)

	s.Equal(stream.Spec.Name, "test")
	s.Equal(len(stream.Spec.Analysers), 1)
	s.Equal(len(stream.Spec.Observers), 1)
}

func (s *ServerTestSuite) TestGetStreamWhenNoStream() {
	req, err := http.NewRequest("GET", "/streams/test", nil)
	s.NoError(err)

	s.sut.ServeHTTP(s.rec, req)

	s.Equal(http.StatusNotFound, s.rec.Code)
}

func (s *ServerTestSuite) TestGetStream() {
	req, err := http.NewRequest("GET", "/streams/test", nil)
	s.NoError(err)

	s.fake.PrependReactor("get", "streams", func(kubetesting.Action) (bool, runtime.Object, error) {
		ret := &v1.Stream{
			TypeMeta: metav1.TypeMeta{
				Kind:       "Stream",
				APIVersion: "vidog.io/v1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      "test",
				Namespace: "testing",
			},
			Spec: v1.StreamSpec{
				Name:      "test",
				Analysers: []string{"motion-detector", "guinea-pig-mood-analyser"},
				Observers: []map[string]string{
					{
						"t":         "email-notifier",
						"eventKind": "motion-detected",
						"email":     "mail@example.com",
					},
				},
			},
		}

		return true, ret, nil
	})
	s.sut.ServeHTTP(s.rec, req)
	s.Equal(http.StatusOK, s.rec.Code)

	st := models.Stream{}
	s.NoError(json.NewDecoder(s.rec.Body).Decode(&st))
	s.Equal(models.Stream{
		Name:      "test",
		VideoUrl:  "/hls/test.m3u8",
		Analysers: []string{"motion-detector", "guinea-pig-mood-analyser"},
		Observers: []map[string]string{
			{
				"t":         "email-notifier",
				"eventKind": "motion-detected",
				"email":     "mail@example.com",
			},
		},
	}, st)
}

func (s *ServerTestSuite) TestDeleteStreamWhenNoStream() {
	req, err := http.NewRequest("DELETE", "/streams/test", nil)
	s.NoError(err)

	s.sut.ServeHTTP(s.rec, req)

	s.Equal(http.StatusGone, s.rec.Code)
}

func (s *ServerTestSuite) TestDeleteStream() {
	req, err := http.NewRequest("DELETE", "/streams/test", nil)
	s.NoError(err)

	s.fake.PrependReactor("delete", "streams", func(kubetesting.Action) (bool, runtime.Object, error) {
		return true, nil, nil
	})

	s.sut.ServeHTTP(s.rec, req)

	s.Equal(http.StatusOK, s.rec.Code)
}

func (s *ServerTestSuite) TestListStreams() {
	req, err := http.NewRequest("GET", "/streams", nil)
	s.NoError(err)

	s.fake.PrependReactor("list", "streams", func(kubetesting.Action) (bool, runtime.Object, error) {
		objs := &v1.StreamList{}
		streams := []v1.Stream{
			{
				TypeMeta: metav1.TypeMeta{
					Kind:       "Stream",
					APIVersion: "vidog.io/v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test",
					Namespace: "testing",
				},
				Spec: v1.StreamSpec{
					Name:      "test",
					Analysers: []string{"motion-detector"},
				},
			},
			{
				TypeMeta: metav1.TypeMeta{
					Kind:       "Stream",
					APIVersion: "vidog.io/v1",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test2",
					Namespace: "testing",
				},
				Spec: v1.StreamSpec{
					Name: "test2",
				},
			},
		}
		objs.Items = append(objs.Items, streams...)
		return true, objs, nil
	})

	s.sut.ServeHTTP(s.rec, req)
	s.Equal(http.StatusOK, s.rec.Code)

	dec := json.NewDecoder(s.rec.Body)
	streams := []models.Stream{}
	s.NoError(dec.Decode(&streams))

	s.Equal(models.Stream{
		Name:     "test",
		VideoUrl: "/hls/test.m3u8",
	}, streams[0])

	s.Equal(models.Stream{
		Name:     "test2",
		VideoUrl: "/hls/test2.m3u8",
	}, streams[1])
}

func TestServerTestSuite(t *testing.T) {
	suite.Run(t, new(ServerTestSuite))
}

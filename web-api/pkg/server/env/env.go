package env

type Env struct {
	Namespace string
	RedisURL string
}
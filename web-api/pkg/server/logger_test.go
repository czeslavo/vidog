package server_test

import (
	"bytes"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/czeslavo/vidog/web-api/pkg/server"
)

func captureOutput(f func()) string {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	f()
	log.SetOutput(os.Stderr)
	return buf.String()
}

func TestLogger(t *testing.T) {
	stubHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	sut := server.Logger(stubHandler)

	req, err := http.NewRequest("GET", "/", nil)
	assert.NoError(t, err)
	rec := httptest.NewRecorder()

	output := captureOutput(func() {
		sut.ServeHTTP(rec, req)
	})

	assert.Equal(t, rec.Code, http.StatusOK)
	assert.Regexp(t, "^.* GET / .*\n", output)
}

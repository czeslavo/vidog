package v1

import metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// Stream is a top level type describing stream instance in the cluster
type Stream struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ObjectMeta `json:"metadata,omitempty"`

	// +optional
	Status StreamStatus `json:"status,omitempty"`
	Spec   StreamSpec   `json:"spec,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// StreamStatus describes current status of the stream
type StreamStatus struct {
	metav1.TypeMeta `json:",inline"`
	IsStreaming     bool `json:"streaming"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// StreamSpec defines stream itself
type StreamSpec struct {
	metav1.TypeMeta `json:",inline"`
	Name            string              `json:"name"`
	Publish         bool                `json:"publish"`
	Analysers       []string            `json:"analysers"`
	Observers       []map[string]string `json:"observers"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

//StreamList is a list of Stream objects
type StreamList struct {
	metav1.TypeMeta `json:",inline"`
	// +optional
	metav1.ListMeta `json:"metadata,omitempty"`

	Items []Stream `json:"items"`
}

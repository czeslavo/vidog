package models_test

import (
	"testing"
	"encoding/json"
	"bytes"
	"github.com/stretchr/testify/assert"

	"gitlab.com/czeslavo/vidog/web-api/pkg/models"
)

func TestStreamJsonDecoding(t *testing.T) {
	input := []byte(`{
		"name": "test",
		"analysers": [
			"motion-detector"
		],
		"observers": [
			{
				"t": "email-notifier",
				"eventKind": "motion-detected",
				"email": "mail@example.com"
			}
		]
	}`)
	decoder := json.NewDecoder(bytes.NewReader(input))
	var stream models.Stream
	err := decoder.Decode(&stream)
	assert.NoError(t, err)
}
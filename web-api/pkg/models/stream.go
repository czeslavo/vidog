package models

type Stream struct {
	Name      string              `json:"name"`
	Analysers []string            `json:"analysers"`
	VideoUrl  string              `json:"video_url,omitempty"`
	Observers []map[string]string `json:"observers"`
}

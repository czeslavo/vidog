package kube

import (
	"os"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	vidog "gitlab.com/czeslavo/vidog/web-api/pkg/client/clientset/versioned"
)

func NewClientsetOrDie(kubeconfig string) (clientset *kubernetes.Clientset) {
	config := configFromFlagsOrDie(kubeconfig)
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	return
}

func NewVidogClientsetOrDie(kubeconfig string) (clientset *vidog.Clientset) {
	config := configFromFlagsOrDie(kubeconfig)
	clientset, err := vidog.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	return
}

func configFromFlagsOrDie(kubeconfig string) (config *rest.Config) {
	if _, err := os.Stat(kubeconfig); os.IsNotExist(err) {
		kubeconfig = "" // will fallback to in-cluster config
	}

	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		panic(err)
	}

	return
}

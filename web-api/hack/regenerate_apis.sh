#!/bin/bash -e

DOCKER_CODEGEN_PKG="/go/src/k8s.io/code-generator"
PROJECT_MODULE="gitlab.com/czeslavo/vidog/web-api"
IMAGE_NAME="appscode/gengo:release-1.14"

CUSTOM_RESOURCE_NAME="vidog"
CUSTOM_RESOURCE_VERSION="v1"

cmd="${DOCKER_CODEGEN_PKG}/generate-groups.sh all \
    "$PROJECT_MODULE/pkg/client" \
    "$PROJECT_MODULE/pkg/apis" \
    $CUSTOM_RESOURCE_NAME:$CUSTOM_RESOURCE_VERSION"

echo "Generating client codes..."
docker run --rm \
           -v "$(pwd):/go/src/${PROJECT_MODULE}" \
           -w "/go/src/${PROJECT_MODULE}" \
           "${IMAGE_NAME}" $cmd

sudo chown $USER:$USER -R ./pkg
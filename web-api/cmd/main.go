package main

import (
	"log"
	"net/http"
	"os"
	"path/filepath"

	"k8s.io/client-go/util/homedir"

	"gitlab.com/czeslavo/vidog/web-api/pkg/kube"
	"gitlab.com/czeslavo/vidog/web-api/pkg/server"
	"gitlab.com/czeslavo/vidog/web-api/pkg/server/env"
)

var (
	kubeconfig = os.Getenv("KUBECONFIG")
	namespace  = os.Getenv("VIDOG_NAMESPACE")
	redisURL = os.Getenv("VIDOG_REDIS_URL")
)

func init() {
	homeConfig := filepath.Join(homedir.HomeDir(), ".kube", "config")
	if _, err := os.Stat(homeConfig); err == nil {
		kubeconfig = homeConfig
	}

	if namespace == "" {
		namespace = "default"
	}
}

func main() {
	log.Printf("Server started, %v, %v, %v", kubeconfig, namespace, redisURL)
	log.Printf("redisURL")
	s := server.NewServer(env.Env{
		Namespace: namespace,
		RedisURL: redisURL,
	},
		kube.NewVidogClientsetOrDie(kubeconfig).VidogV1(),
		kube.NewClientsetOrDie(kubeconfig))
	log.Fatal(http.ListenAndServe(":8000", s))
}

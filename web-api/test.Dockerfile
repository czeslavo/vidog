FROM golang:1.12.4 as builder

WORKDIR /app
ADD go.mod go.sum ./
RUN go mod download

ADD . .
RUN go test ./...

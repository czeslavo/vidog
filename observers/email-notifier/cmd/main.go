package main

import (
	"gitlab.com/czeslavo/vidog/observers/email-notifier/notifier"
)

func main() {
	notifier.Run()
}
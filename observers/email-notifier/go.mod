module gitlab.com/czeslavo/vidog/observers/email-notifier

go 1.12

require (
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/stretchr/testify v1.3.0
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gopkg.in/mail.v2 v2.3.1
)

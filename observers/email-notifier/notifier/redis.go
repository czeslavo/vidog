package notifier

import (
	"encoding/json"
	"log"

	"github.com/gomodule/redigo/redis"

	"gitlab.com/czeslavo/vidog/observers/email-notifier/config"
)

func Run() {
	for {
		conn, err := redis.Dial("tcp", config.Redis.URL)
		if err != nil {
			log.Printf("Couldn't connect to redis, %v", err)
			continue
		}

		psConn := redis.PubSubConn{Conn: conn}
		psConn.Subscribe(config.Redis.Topic)

		for {
			switch v := psConn.Receive().(type) {
			case redis.Message:
				var ev Event
				err := json.Unmarshal(v.Data, &ev)
				if err != nil {
					log.Printf("Error parsing event %v", err)
					continue
				}

				log.Printf("Received from redis: %v", ev)
				if ev.Kind == config.Redis.EventKind {
					sendEmail(ev)
				}
			case redis.Subscription:
				log.Printf("%s: %s %d", v.Channel, v.Kind, v.Count)
			case error:
				log.Printf("Error from redis, %s", v.Error())
				if err := conn.Err(); err != nil {
					log.Printf("Fatal connection error, %v", err)
					return
				}
			}
		}
	}
}

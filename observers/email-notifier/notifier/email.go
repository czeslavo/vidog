package notifier

import (
	"encoding/json"
	"fmt"
	"log"

	"gitlab.com/czeslavo/vidog/observers/email-notifier/config"
	gomail "gopkg.in/mail.v2"
)

func sendEmail(event Event) {
	subject := fmt.Sprintf("[%s] Event detected", event.Kind)

	body := generateEmailBody(event)

	m := gomail.NewMessage()
	m.SetHeader("From", config.Email.From)
	m.SetHeader("To", config.Email.To...)
	m.SetHeader("Subject", subject)
	m.SetBody("text/html", body)

	d := gomail.NewDialer(config.Email.SMTPHost, config.Email.SMTPPort, config.Email.User, config.Email.Password)
	if err := d.DialAndSend(m); err != nil {
		log.Printf("Error sending email, %v", err)
	}
}

func generateEmailBody(event Event) (body string) {
	if event.Kind == "face-detected" {
		for k, v := range event.Data {
			if k == "snapshotPath" {
				snapshotPath, ok := v.(string)
				if ok {
					body = fmt.Sprintf(`<h1>Motion detected</h1><img src="https://vidog.podlipie.net` + snapshotPath + `" alt="snapshot" />`)
					return
				}
			}
		}
	}

	js, err := json.Marshal(event)
	if err != nil {
		log.Printf("Couldn't serialize event to JSON, %v", err)
	}

	body = fmt.Sprintf("Event has been detected: %s", js)
	return
}

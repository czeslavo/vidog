package notifier

type Event struct {
	Kind    string                 `json:"kind"`
	Time    float64                `json:"time"`
	Message string                 `json:"message"`
	Data    map[string]interface{} `json:"data"`
}

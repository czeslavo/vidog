package config

import (
	"log"
	"os"
	"strconv"
	"strings"
)

var Email struct {
	From string
	To   []string

	SMTPHost string
	SMTPPort int

	User     string
	Password string
}

func init() {
	Email.From = os.Getenv("VIDOG_EMAIL_FROM")
	Email.SMTPHost = os.Getenv("VIDOG_EMAIL_SMTP_HOST")
	Email.SMTPPort = getSMTPPort()
	Email.User = os.Getenv("VIDOG_EMAIL_USER")
	Email.Password = os.Getenv("VIDOG_EMAIL_PASSWORD")

	// from user
	Email.To = getTo()
}

func getTo() []string {
	return strings.Split(os.Getenv("VIDOG_EMAIL_TO"), ",")
}

func getSMTPPort() int {
	i, err := strconv.Atoi(os.Getenv("VIDOG_EMAIL_SMTP_PORT"))
	if err != nil {
		log.Fatalf("Invalid port, %v", err)
	}
	return i
}

package config

import (
	"os"
)

var Redis struct {
	URL       string
	Topic     string
	EventKind string
}

func init() {
	Redis.URL = os.Getenv("VIDOG_REDIS_URL")
	Redis.Topic = os.Getenv("VIDOG_REDIS_TOPIC")

	// from user
	Redis.EventKind = os.Getenv("VIDOG_REDIS_EVENT_KIND")
}
